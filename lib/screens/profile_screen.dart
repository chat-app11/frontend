import 'package:flutter/material.dart';
import 'package:frontend/authentication/auth.dart';
import 'package:frontend/responsive/style.dart';

class ProfileScreen extends StatefulWidget {
  static const routeName = 'profile-screen';

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  bool editProfileName = false;
  bool editProfileEmail = false;
  bool editProfileAddress = false;

  String name = 'Anas Rasheed';
  // String email = 'anasrasheed237@gmail.com';
  String address = '1/23 sector 11-F New Karachi';

  bool init = true;
  @override
  Widget build(BuildContext context) {
    // User user;
    // if (init) {
    //   final id = ModalRoute.of(context).settings.arguments as String;
    //   user = Provider.of<UserProvider>(context).userById(id);
    //   init = false;
    // }
    final mediaQueryHeight = MediaQuery.of(context).size.height;
    void _editProfile(int value) {
      if (value == 1) editProfileName = true;
      if (value == 2) editProfileEmail = true;
      if (value == 3) editProfileAddress = true;
      setState(() {});
    }

    void cancelEditing(int value) {
      if (value == 1) editProfileName = false;
      if (value == 2) editProfileEmail = false;
      if (value == 3) editProfileAddress = false;
      setState(() {});
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: active.withOpacity(.6),
        title: Text('Profile'),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: (mediaQueryHeight / 15)),
                child: Stack(
                  fit: StackFit.loose,
                  alignment: Alignment.bottomRight,
                  children: [
                    CircleAvatar(
                      maxRadius: mediaQueryHeight * 0.13,
                      backgroundColor: active.withOpacity(.6),
                      child: Icon(
                        Icons.account_box_rounded,
                        size: mediaQueryHeight * 0.13,
                      ),
                    ),
                    IconButton(
                        icon: Icon(Icons.edit_outlined),
                        color: Theme.of(context).primaryColor,
                        onPressed: () {}),
                  ],
                ),
              ),
              editProfileName
                  ? editProfileWidget(
                      context,
                      nameController,
                      () {
                        cancelEditing(1);
                      },
                      () {
                        editProfileName = false;
                        print(nameController.text);
                        name = nameController.text;

                        setState(() {});
                      },
                    )
                  : buildProfileDetail(
                      context: context,
                      heading: 'Name',
                      headingText: nameController.text == ""
                          ? "$username"
                          : nameController.text,
                      onPressed: () {
                        _editProfile(1);
                      },
                    ),
              editProfileEmail
                  ? editProfileWidget(
                      context,
                      emailController,
                      () {
                        cancelEditing(2);
                      },
                      () {
                        editProfileEmail = false;
                        email = emailController.text;
                        print(email);
                      },
                    )
                  : buildProfileDetail(
                      context: context,
                      heading: 'Email',
                      headingText: emailController.text == ""
                          ? "$email"
                          : emailController.text,

                      // onPressed: () {
                      //   _editProfile(2);
                      // },
                    ),
              editProfileAddress
                  ? editProfileWidget(
                      context,
                      addressController,
                      () {
                        cancelEditing(3);
                      },
                      () {
                        editProfileAddress = false;
                        address = addressController.text;
                        setState(() {});
                        print(address);
                      },
                    )
                  : buildProfileDetail(
                      context: context,
                      heading: 'Address',
                      headingText: addressController.text == ""
                          ? "Street 12 house 14"
                          : addressController.text,
                      onPressed: () {
                        _editProfile(3);
                      },
                    ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildProfileDetail(
      {BuildContext context,
      String heading,
      String headingText,
      Function onPressed}) {
    var mediaQueryHeight = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(left: mediaQueryHeight / 90),
      margin: EdgeInsets.only(top: (mediaQueryHeight / 90)),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey[100].withOpacity(1),
            spreadRadius: 5.0,
            blurRadius: 1.0,
          )
        ],
        color: Colors.white70,
        borderRadius: BorderRadius.circular(mediaQueryHeight / 80),
      ),
      width: MediaQuery.of(context).size.width * 0.9,
      height: MediaQuery.of(context).size.height * 0.11,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                heading,
                style: TextStyle(fontSize: mediaQueryHeight / 65),
              ),
              IconButton(
                icon: Icon(Icons.edit_outlined),
                color: Theme.of(context).primaryColor,
                onPressed: onPressed,
              )
            ],
          ),
          Text(
            headingText,
            style: TextStyle(
              fontSize: mediaQueryHeight / 50,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}

Widget editProfileWidget(BuildContext context, TextEditingController controller,
    Function onPressed, Function onSave) {
  final mediaQueryHeight = MediaQuery.of(context).size.height;
  final mediaQueryWidth = MediaQuery.of(context).size.width;

  return Container(
    padding: EdgeInsets.only(left: mediaQueryHeight / 90),
    margin: EdgeInsets.only(top: (mediaQueryHeight / 90)),
    decoration: BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: Colors.grey[100].withOpacity(1),
          spreadRadius: 5.0,
          blurRadius: 1.0,
        )
      ],
      color: Colors.white70,
      borderRadius: BorderRadius.circular(mediaQueryHeight / 80),
    ),
    width: mediaQueryWidth * 0.9,
    height: mediaQueryHeight * 0.13,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.8,
          child: TextFormField(
            controller: controller,
          ),
        ),
        ButtonBar(
          buttonPadding: EdgeInsets.all(5),
          children: [
            TextButton(onPressed: onPressed, child: Text('Cancel')),
            TextButton(onPressed: onSave, child: Text('Save')),
          ],
        )
      ],
    ),
  );
}
