import 'package:flutter/material.dart';
import 'package:frontend/authentication/auth.dart';
import 'package:frontend/controller/chat-controller.dart';
import 'package:frontend/controller/model/message.dart';
import 'package:frontend/model/group.dart';
import 'package:frontend/model/user.dart';
import 'package:frontend/responsive/style.dart';
import 'package:frontend/widgets/reply_message_widget.dart';
import 'package:frontend/widgets/send_message_widget.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:image_picker/image_picker.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:socket_io_client/socket_io_client.dart' as IO;

class GroupChatScreen extends StatefulWidget {
  static const routeName = "group-chat-screen";
  @override
  _GroupChatScreenState createState() => _GroupChatScreenState();
}

class _GroupChatScreenState extends State<GroupChatScreen> {
  final TextEditingController msgInputController = TextEditingController();

  final ItemScrollController _scrollController = ItemScrollController();
  ChatController chatController = ChatController();
  IO.Socket socket;
  @override
  void initState() {
    socket = IO.io(
        'http://127.0.0.1:4000',
        IO.OptionBuilder()
            .setTransports(['websocket'])
            .disableAutoConnect()
            .build());
    socket.connect();
    socket.emit("signin", authId);
    socket.onConnect((data) {
      print('connected');
      socket.on('msg', (data) {
        print(data);
        var messageJson = {
          "message": data['msg'],
          "sentByMe": data['authId'],
        };
        chatController.chatMessages.add(Message.fromJson(messageJson));
        print(chatController.chatMessages);
      });
    });
    // setUpSocketListener();
    super.initState();
  }

  void sendMsg(String message, String sourceId, String targetId) {
    socket.emit("msg", {
      "msg": message,
      "sourceId": sourceId,
      "targetId": targetId,
    });
    var messageJson = {
      "message": message,
      "sentByMe": authId,
    };
    chatController.chatMessages.add(Message.fromJson(messageJson));
  }

  @override
  Widget build(BuildContext context) {
    final id = ModalRoute.of(context).settings.arguments as String;
    final group = Provider.of<GroupProvider>(context).groupById(id);
    // Provider.of<MessageProvider>(context).connect();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: active.withOpacity(.7),
        title: ListTile(
          onTap: () {
            showDialog(
                context: context,
                builder: (ctx) => AlertDialog(
                    title: Text("Group Members"),
                    content: Container(
                      height: 200,
                      width: 200,
                      child: ListView.builder(
                        itemBuilder: (ctx, i) => ListTile(
                          leading: CircleAvatar(
                            backgroundColor: active.withOpacity(.6),
                          ),
                          title: Text(
                            group.users.values.toList()[i].username,
                          ),
                        ),
                        itemCount: group.users.values.toList().length,
                      ),
                    )));
          },
          contentPadding: EdgeInsets.zero,
          title: Text(
            "${group.name}",
            style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
              letterSpacing: 2,
            ),
          ),
          subtitle: Text("${group.users.length} Members"),
        ),
        actions: [
          appBarButton(
            icon: Icons.call,
            onPressed: () {},
          ),
          appBarButton(
            icon: Icons.video_call_sharp,
            onPressed: () {},
          ),
          appBarButton(
            icon: Icons.more_vert,
            onPressed: () {},
          ),
        ],
      ),
      // resizeToAvoidBottomInset: false,
      body: Container(
        color: Colors.grey[100],
        child: Column(
          children: [
            Expanded(
              // height: MediaQuery.of(context).size.height - 140,
              // color: lightGrey.withOpacity(.3),
              child: Obx(
                () => ScrollablePositionedList.builder(
                  itemBuilder: (ctx, i) {
                    var currentItem = chatController.chatMessages[i];
                    if (currentItem.sentByMe == authId)
                      return SendMessageWidget(
                        message: currentItem.message,
                        time: timeago.format(DateTime.now()),
                      );
                    return ReplyMessageWidget(
                        message: currentItem.message,
                        time: timeago.format(DateTime.now()));
                  },
                  itemCount: chatController.chatMessages.length,
                  itemScrollController: _scrollController,
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 2,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: "Type a ChatMessage",
                        hintStyle: TextStyle(fontSize: 14),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                      ),
                      onTap: () {
                        // _scrollController.scrollTo(
                        //   index: chatController.chatMessages.length + 1,
                        //   duration: Duration(microseconds: 1),
                        // );
                        // print(context.read<MessageProvider>().list.length);
                      },
                      onEditingComplete: () {
                        // _saveMsg(context);
                        print(chatController.chatMessages.length);
                        // sendMessage(msgInputController.text);
                        // sendMsg(msgInputController.text, authId, user.id);
                        msgInputController.text = "";
                      },
                      controller: msgInputController,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (ctx) => FileDialog(),
                      );
                    },
                    child: Icon(
                      Icons.attach_file,
                      color: active.withOpacity(.6),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      // sendMessage(msgInputController.text);
                      // sendMsg(msgInputController.text, authId, user.id);
                      msgInputController.text = "";
                    },
                    child: Text(
                      'Send',
                      style: TextStyle(
                          color: active.withOpacity(.6),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  IconButton appBarButton({IconData icon, Function onPressed}) {
    return IconButton(
      onPressed: onPressed,
      icon: Icon(icon),
      // padding: EdgeInsets.zero,
      iconSize: 24,
      splashRadius: 25,
      constraints: BoxConstraints(
        maxWidth: 35,
      ),
    );
  }

  // _saveMsg(BuildContext context) {
  //   final _msg = Provider.of<MessageProvider>(context, listen: false);
  //   FocusScope.of(context).unfocus();
  //   _msg.addMsg(
  //     Message(
  //       "Rehan",
  //       "10:59 PM",
  //       msgInputController.text,
  //     ),
  //   );
  //   _msg.sendChatToServer(msgInputController.text, "2");
  //   msgInputController.text = "";
  //   _scrollController.scrollTo(
  //     index: _msg.list.length,
  //     duration: Duration(microseconds: 1),
  //   );
  // }

  void sendMessage(String text) {
    var messageJson = {
      "message": text,
      "sentByMe": socket.id,
    };
    FocusScope.of(context).unfocus();
    _scrollController.scrollTo(
      index: chatController.chatMessages.length,
      duration: Duration(microseconds: 1),
    );
    socket.emit('message', messageJson);
    chatController.chatMessages.add(Message.fromJson(messageJson));
  }

  void setUpSocketListener() {
    socket.on('message-receive', (data) {
      print(data);
      chatController.chatMessages.add(Message.fromJson(data));
    });
  }
}

class FileDialog extends StatelessWidget {
  const FileDialog({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        height: 100,
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            TextButton(
              onPressed: () async {
                final ImagePicker _picker = ImagePicker();
                final XFile image =
                    await _picker.pickImage(source: ImageSource.gallery);
                print("jj" + image.path);
              },
              child: Text("Choose Image"),
            ),
          ],
        ),
      ),
    );
  }
}

class ChatMessage extends StatelessWidget {
  final String name;
  final String time;
  final String message;

  const ChatMessage(this.name, this.time, this.message);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Row(
        children: [
          CircleAvatar(
            radius: 20,
            backgroundColor: Colors.grey,
          ),
          SizedBox(width: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  FittedBox(
                    child: Text(
                      name,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(width: 5),
                  FittedBox(
                    child: Text(
                      time,
                      style: TextStyle(color: Colors.grey, fontSize: 14),
                    ),
                  ),
                ],
              ),
              Container(
                width: 330,
                child: Text(message),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
