import 'package:flutter/material.dart';
import 'package:frontend/authentication/auth.dart';
import 'package:frontend/model/group.dart';
import 'package:frontend/model/user.dart';
import 'package:frontend/responsive/style.dart';
import 'package:frontend/screens/profile_screen.dart';
import 'package:frontend/widgets/user_tile_widget.dart';
import 'package:provider/provider.dart';

import 'add_user_to_group.dart';
import 'chat_screen.dart';
import 'group_chat_screen.dart';

// ignore: must_be_immutable
class UsersScreen extends StatelessWidget {
  Future<void> _refresh(BuildContext context) async {
    await Provider.of<UserProvider>(context, listen: false).getUsers();
  }

  bool loadData = true;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(
                child: Text("User"),
              ),
              Tab(
                child: Text("Group"),
              ),
            ],
          ),
          backgroundColor: active.withOpacity(.6),
          title: Text("Chat App".toUpperCase()),
          actions: [
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.search),
              splashRadius: 25,
            ),
            IconButton(
              onPressed: () {
                Navigator.pushNamed(context, ProfileScreen.routeName,
                    arguments: authId);
              },
              icon: Icon(Icons.person),
              splashRadius: 25,
              tooltip: "Profile",
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: active.withOpacity(.6),
          child: Icon(
            Icons.group_add,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pushNamed(context, AddUserToGroup.routeName);
          },
        ),
        body: TabBarView(
          children: [
            Container(
              padding: EdgeInsets.only(top: 8),
              color: lightGrey.withOpacity(.3),
              child: FutureBuilder(
                future: _refresh(context),
                builder: (context, snapshot) {
                  // if (snapshot.connectionState == ConnectionState.waiting)
                  //   return Center(child: CircularProgressIndicator());
                  // else if (snapshot.hasError)
                  //   return Center(
                  //     child: Column(
                  //       children: [
                  //         Text('Error Loading Data'),
                  //         TextButton(
                  //           onPressed: () {
                  //             _refresh(context);
                  //           },
                  //           child: Text('retry'),
                  //         )
                  //       ],
                  //     ),
                  //   );
                  return RefreshIndicator(
                    onRefresh: () => _refresh(context),
                    child: Consumer<UserProvider>(
                      builder: (_, userList, __) => ListView.builder(
                        itemBuilder: (ctx, i) => UserTileWidget(
                          name: userList.users[i].username,
                          onTap: () {
                            Navigator.pushNamed(context, ChatScreen.routeName,
                                arguments: userList.users[i].id);
                          },
                        ),
                        itemCount: userList.users.length,
                      ),
                    ),
                  );
                },
              ),
            ),
            Container(
              child: RefreshIndicator(
                onRefresh: () => _refresh(context),
                child: Consumer<GroupProvider>(
                  builder: (_, group, __) => ListView.builder(
                    itemBuilder: (ctx, i) => UserTileWidget(
                      name: group.list[i].name,
                      onTap: () {
                        Navigator.pushNamed(context, GroupChatScreen.routeName,
                            arguments: group.list[i].id);
                      },
                    ),
                    itemCount: group.list.length,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
