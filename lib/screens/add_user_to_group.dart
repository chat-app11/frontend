import 'package:flutter/material.dart';
import 'package:frontend/model/group.dart';
import 'package:frontend/model/user.dart';
import 'package:frontend/model/user_list_group.dart';
import 'package:frontend/responsive/style.dart';
import 'package:frontend/widgets/user_tile_widget.dart';
import 'package:provider/provider.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class AddUserToGroup extends StatelessWidget {
  final ItemScrollController _scrollController = ItemScrollController();
  final _groupName = TextEditingController();
  static const routeName = "select-contact";
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: active.withOpacity(.6),
        title: ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text(
            "New Group",
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                wordSpacing: 4),
          ),
          subtitle: Text(
            context.watch<UserProvider>().users.length.toString() + " Contacts",
            style: TextStyle(
              color: Colors.white,
              wordSpacing: 4,
            ),
          ),
        ),
        actions: [
          IconButton(onPressed: () {}, icon: Icon(Icons.search)),
          IconButton(onPressed: () {}, icon: Icon(Icons.more_vert)),
        ],
      ),
      backgroundColor: lightGrey.withOpacity(.3),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: Container(
              height: 150,
              // margin: EdgeInsets.only(top: 100, left: 50),
              // width: 300,
              // height: height * 0.11,

              child: Consumer<UserListGroup>(
                builder: (context, userList, _) => userList.list.length == 0
                    ? Center(child: Text("Select Users from list"))
                    : ScrollablePositionedList.builder(
                        itemScrollController: _scrollController,
                        itemBuilder: (ctx, i) => Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 0.005 * height, horizontal: 8),
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Provider.of<UserListGroup>(context,
                                          listen: false)
                                      .removeUserFromList(
                                          userList.list.keys.toList()[i]);
                                  ScaffoldMessenger.of(context)
                                      .hideCurrentSnackBar();
                                  ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(content: Text("User removed")));
                                },
                                child: Stack(
                                  children: [
                                    CircleAvatar(
                                      child: Text(
                                          "${userList.list.values.toList()[i].username.substring(0, 1)}"),
                                      backgroundColor: active.withOpacity(.6),
                                    ),
                                    Positioned(
                                      top: 30,
                                      left: 30,
                                      child: Container(
                                        width: 10,
                                        height: 10,
                                        child: Icon(
                                          Icons.cancel,
                                          size: 14,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Text(
                                "${userList.list.values.toList()[i].username}",
                                style:
                                    TextStyle(fontSize: 12, color: lightBlack),
                              ),
                            ],
                          ),
                        ),
                        itemCount: userList.list.length,
                        scrollDirection: Axis.horizontal,
                      ),
              ),
            ),
          ),
          Container(height: 1, color: lightGrey),
          Expanded(
            flex: 7,
            child: Container(
              margin: EdgeInsets.only(top: 0),
              padding: EdgeInsets.zero,
              // color: lightGrey.withOpacity(.3),
              child: Consumer<UserProvider>(
                builder: (_, userList, __) => ListView.builder(
                  itemBuilder: (ctx, i) {
                    return UserTileWidget(
                      name: userList.users[i].username,
                      selected: Provider.of<UserListGroup>(context)
                          .userById(int.parse(userList.users[i].id)),
                      onTap: () {
                        var user =
                            Provider.of<UserProvider>(context, listen: false)
                                .userById(userList.users[i].id);
                        Provider.of<UserListGroup>(context, listen: false)
                            .addUserToGroup(user);
                        _scrollController.scrollTo(
                            index: context
                                    .read<UserListGroup>()
                                    .list
                                    .values
                                    .toList()
                                    .length +
                                1,
                            duration: Duration(microseconds: 1));
                      },
                    );
                  },
                  itemCount: userList.users.length,
                ),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: active.withOpacity(.6),
        child: Icon(Icons.arrow_forward),
        onPressed: () {
          if (context.read<UserListGroup>().list.length > 1) {
            showDialog(
                context: context,
                builder: (ctx) => AlertDialog(
                      title: Text("Enter Group Name"),
                      content: Container(
                        width: 200,
                        child: TextField(
                          decoration: InputDecoration(labelText: "Group Name"),
                          controller: _groupName,
                        ),
                      ),
                      actions: [
                        TextButton(
                          onPressed: () {
                            if (_groupName.text != "") {
                              var groupUser = context.read<UserListGroup>();
                              Provider.of<GroupProvider>(context, listen: false)
                                  .addGroup(_groupName.text, groupUser.list);
                              groupUser.removeAllUser();
                              Navigator.pop(context);
                            } else {
                              snakbar(context, "Enter Group Name");
                            }
                          },
                          child: Text("Create"),
                        ),
                      ],
                    ));
          } else {
            snakbar(context, "Select at least two Users");
          }
        },
      ),
    );
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> snakbar(
      BuildContext context, String text) {
    return ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text("$text")));
  }
}
