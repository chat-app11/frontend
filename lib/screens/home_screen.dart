// import 'package:flutter/material.dart';
// import 'package:frontend/authentication/auth.dart';
// import 'package:frontend/responsive/style.dart';
// import 'package:provider/provider.dart';

// class HomePage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final userData = Provider.of<Auth>(context);
//     return Scaffold(
//       body: Center(
//         child: Container(
//           alignment: Alignment.center,
//           margin:
//               EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.17),
//           padding: EdgeInsets.all(20),
//           width: 330,
//           height: 200,
//           decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(20),
//             color: Colors.grey.shade200.withOpacity(0.7),
//           ),
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Text(
//                 "Welcome To Home",
//                 style: TextStyle(
//                   letterSpacing: 4,
//                   fontSize: 32,
//                   fontWeight: FontWeight.bold,
//                   color: lightBlack,
//                 ),
//               ),
//               dataText('Username', userData.username),
//               dataText('User Id ', userData.userId),
//               dataText('email', userData.email),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   Text dataText(String heading, String userData) {
//     return Text(
//       '$heading :- ${userData}',
//       style: const TextStyle(
//           fontFamily: 'Lato', fontSize: 16, fontWeight: FontWeight.bold),
//     );
//   }
// }
