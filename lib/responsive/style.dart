import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

Color light = Color(0xFFF7F8FC);
Color lightGrey = Color(0xFFA4A6B3);
Color dark = Color(0xFF363740);
Color active = Color(0xFF3C19C0);
Color lightBlack = Colors.black.withOpacity(0.7);
Color lightYellow = Colors.yellow.shade400.withOpacity(.7);
