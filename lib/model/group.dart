import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:frontend/model/user.dart';

class Group {
  final String id;
  final String name;
  final Map<int, User> users;

  Group(this.id, this.name, this.users);
}

class GroupProvider with ChangeNotifier {
  List<Group> _list = [
    Group("1", "Universal group", UserProvider().users.asMap())
  ];

  List<Group> get list {
    return [..._list];
  }

  Group groupById(String id) {
    return _list.firstWhere((group) => group.id == id);
  }

  void addGroup(String name, Map<int, User> users) {
    _list.add(Group("3", name, users));
    notifyListeners();
    // print(_list.last.users.values.last.username);
  }
}
