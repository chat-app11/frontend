import 'package:flutter/foundation.dart';
import 'package:frontend/model/user.dart';

class UserListGroup with ChangeNotifier {
  Map<int, User> _list = {};
  Map<int, User> get list {
    return {..._list};
  }

  bool userById(int id) {
    return _list.containsKey(id);
  }

  void addUserToGroup(User user) {
    if (_list.containsKey(int.parse(user.id))) {
      _list.remove(int.parse(user.id));
      print("yes");
    } else {
      _list.putIfAbsent(
        int.parse(user.id),
        () => User(
          user.username,
          user.email,
          id: user.id,
        ),
      );
    }
    print(_list.values);
    notifyListeners();
  }

  void removeUserFromList(int id) {
    _list.remove(id);
    notifyListeners();
  }

  void removeAllUser() {
    _list = {};
    notifyListeners();
  }
}
