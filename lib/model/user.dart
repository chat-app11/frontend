import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:frontend/authentication/auth.dart';
import 'package:http/http.dart' as http;

class User {
  final String id;
  final String username;
  final String email;
  final String image;

  User(this.username, this.email, {this.image = "", this.id = ""});
}

class UserProvider with ChangeNotifier {
  List<User> _users = [
    User("anas", "anas@gmail.com", id: "1"),
    User("Rehan", "anas@gmail.com", id: "3"),
    User("ali", "anas@gmail.com", id: "4"),
    User("asif", "anas@gmail.com", id: "5"),
    User("ahmed", "anas@gmail.com", id: "6"),
    User("raza", "anas@gmail.com", id: "7"),
    User("abbas", "anas@gmail.com", id: "8"),
    User("asim", "anas@gmail.com", id: "9"),
  ];

  List<User> get users {
    return [..._users];
  }

  User userById(String id) {
    return _users.firstWhere((user) => user.id == id);
  }

  Future<void> getUsers() async {
    print("auth id");
    print(authId);
    final url = 'http://127.0.0.1:8000/api/conversation/$authId';
    try {
      final response = await http
          .get(Uri.parse(url), headers: {'Authorization': "Bearer $authToken"});
      // print(response.body);
      final decodedData = jsonDecode(response.body) as Map<String, dynamic>;
      _users = [];
      for (int i = 0; i < decodedData['users'].length; i++) {
        _users.add(
          User(
            decodedData['users'][i]['name'],
            decodedData['users'][i]['email'],
            id: decodedData['users'][i]['id'].toString(),
          ),
        );
      }
      notifyListeners();
    } catch (e) {
      print("Error from Users");
      print(e);
    }
  }
}
