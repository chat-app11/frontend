import 'package:flutter/foundation.dart';
import 'package:socket_io_client/socket_io_client.dart';

class Message {
  final String id;
  final String text;
  final String sender;
  final String time;
  final int type;

  Message(this.sender, this.time, this.text, {this.type = 0,this.id});
}

class MessageProvider extends ChangeNotifier {
  Socket socket;
  List<Message> _list = [
    Message("Anas Rasheed", "10:52 PM", "heloo ", type: 0),
    Message("Anas Rasheed", "10:52 PM",
        "heloo how are you I am fine what o tell u tommorow",
        type: 0),
    Message("Anas Rasheed", "10:52 PM",
        "heloo how are you I am fine what ng i forgot to tell u tommorow",
        type: 0),
    Message("Anas Rasheed", "10:52 PM",
        "heloo how hing i forgot to tell u tommorow",
        type: 1),
    Message("Anas Rasheed", "10:52 PM", "heloo  u tommorow", type: 1),
    Message(
      "Anas Rasheed",
      "10:52 PM",
      "heloo hi forgot to tell u tommorow",
    ),
    Message("Anas Rasheed", "10:52 PM", "tommorow", type: 1),
    Message(
      "Anas Rasheed",
      "10:52 PM",
      "heloo ",
    ),
    Message("Anas Rasheed", "10:52 PM",
        "heloo how are you I am fine what o tell u tommorow",
        type: 1),
    Message(
      "Anas Rasheed",
      "10:52 PM",
      "heloo how are you I am fine what ng i forgot to tell u tommorow",
    ),
    Message("Anas Rasheed", "10:52 PM",
        "heloo how hing i forgot to tell u tommorow",
        type: 1),
    Message(
      "Anas Rasheed",
      "10:52 PM",
      "heloo  u tommorow",
    ),
    Message("Anas Rasheed", "10:52 PM", "heloo hi forgot to tell u tommorow",
        type: 1),
    Message(
      "Anas Rasheed",
      "10:52 PM",
      "tommorow",
    ),
    Message("Anas Rasheed", "10:52 PM", "tommorow", type: 1),
    Message(
      "Anas Rasheed",
      "10:52 PM",
      "heloo ",
    ),
    Message("Anas Rasheed", "10:52 PM",
        "heloo how are you I am fine what o tell u tommorow",
        type: 1),
    Message(
      "Anas Rasheed",
      "10:52 PM",
      "heloo how are you I am fine what ng i forgot to tell u tommorow",
    ),
  ];

  List<Message> get list {
    return [..._list];
  }

  void addMsg(Message msg) {
    _list.add(msg);
    notifyListeners();
  }

  void connect() {
    socket = io("http://127.0.0.1:3000", {
      "transports": ["websocket"],
      "autoConnect": false,
    });
    socket.connect();
    socket.onConnect((_) => {print("connected")});
    print(socket.connected);
  }

  void sendChatToServer(String msg,String id) {
    socket.emit("sendChatToServer", {"message": msg, "id": id});
    socket.on("sendChatToClient", (data) => print(data));
  }
}
