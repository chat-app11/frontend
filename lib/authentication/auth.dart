import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

String authToken;
String authId;
String username;
String email;

class Auth with ChangeNotifier {
  String _token;
  String _userId;
  String _email;
  String _username;

  bool get isAuth {
    return _token != '';
  }

  // String get email {
  //   return _email;
  // }

  // String get username {
  //   return _username;
  // }

  String get userId {
    return _userId;
  }

  String get token {
    if (token != null) return _token;

    return null;
  }

  Future<bool> _authenticate(String urlSegment, Object body) async {
    final url = 'http://127.0.0.1:8000/api/$urlSegment';

    try {
      final response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          // 'Accept': 'application/json'
        },
        body: jsonEncode(body),
      );
      // print(response.statusCode);
      final decodedData = jsonDecode(response.body);
      // print(decodedData);
      if (decodedData['msg'] != null) {
        print(decodedData['msg'] + "jjj");
        throw new Exception(decodedData['msg']);
      }
      authToken = decodedData['token'];
      authId = decodedData['user']['id'].toString();
      username = decodedData['user']['name'];
      email = decodedData['user']['email'];
      print(authId);
      notifyListeners();
      return true;
    } catch (error) {
      // print("Error from Auth");
      // print(error);
      return false;
      // throw error;
    }
    // _token = decodedData['token'];
  }

  Future<void> register(String username, String email, String password) async {
    _authenticate(
      'register/',
      {
        'name': username,
        'email': email,
        'password': password,
        'password_confirmation': password,
      },
    );
  }

  Future<void> login(String email, String password) async {
    _authenticate(
      "login/",
      {
        'email': email,
        'password': password,
      },
    );
  }
}
