class Message {
  final String message;
  final String sentByMe;

  Message(this.message, this.sentByMe);

  factory Message.fromJson(Map<String,dynamic> json ){
    return Message(json['message'], json['sentByMe']);
  }
}
