import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ReplyMessageWidget extends StatelessWidget {
  final String message;
  final String time;

  const ReplyMessageWidget({Key key, this.message, this.time})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: ConstrainedBox(
        constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width - 55, minWidth: 100),
        child: Card(
          elevation: 1,
          color: Colors.grey[350],
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Stack(
            children: [
              Padding(
                padding:
                    EdgeInsets.only(bottom: 22, right: 10, left: 10, top: 5),
                child: Text(message,
                    style: GoogleFonts.b612(
                      color: Colors.black.withOpacity(.7),
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    )),
              ),
              Positioned(
                right: 8,
                bottom: 4,
                child: Text(
                  time,
                  style: TextStyle(
                    color: Colors.black45,
                    fontSize: 13,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
