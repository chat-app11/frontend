import 'package:flutter/material.dart';
import 'package:frontend/responsive/style.dart';
import 'package:google_fonts/google_fonts.dart';

class SendMessageWidget extends StatelessWidget {
  final String time;
  final String message;

  const SendMessageWidget({this.time, this.message});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width - 45,
          minWidth: 100,
        ),
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          color: active.withOpacity(.7),
          child: Stack(
            children: [
              Padding(
                padding:
                    EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 22),
                child: Text(
                  message,
                  style: GoogleFonts.b612(
                    color: Colors.white,
                    fontSize: 16,
                    // fontWeight: FontWeight.w300,
                  ),
                ),
              ),
              Positioned(
                  right: 8,
                  bottom: 4,
                  child: Text(
                    time,
                    style: TextStyle(
                      fontSize: 13,
                      color: Colors.grey,
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
 