import 'package:flutter/material.dart';
import 'package:frontend/authentication/auth.dart';
import 'package:frontend/responsive/style.dart';
import 'package:frontend/screens/users_screen.dart';
import 'package:provider/provider.dart';

import 'custom_btn.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  var register = false;
  Map<String, String> _formData = {
    "username": "ahmed",
    "email": "",
    "password": "",
  };
  final _passwordController = TextEditingController();
  GlobalKey<FormState> _formKey = GlobalKey();
  bool _buttonPressed = false;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            register ? "register".toUpperCase() : "Login".toUpperCase(),
            style: TextStyle(
              letterSpacing: 4,
              fontSize: 32,
              fontWeight: FontWeight.bold,
              color: lightBlack,
            ),
          ),
          customTextField(
            "Email",
            Icons.email,
            onSaved: (val) {
              _formData['email'] = val;
            },
          ),
          customTextField(
            "Password",
            Icons.password,
            onSaved: (val) {
              _formData['password'] = val;
            },
          ),
          if (register)
            customTextField(
              "Confirm Password",
              Icons.password,
              controller: _passwordController,
            ),
          _buttonPressed
              ? CircularProgressIndicator()
              : CustomBtn(
                  text: register ? "Register" : "Login", onPressed: onPressed),
          SizedBox(height: 15),
          Column(
            children: [
              Text(
                register ? "Already have An Account" : "Don't have Account",
                style: TextStyle(color: lightBlack, fontSize: 14),
              ),
              TextButton(
                onPressed: () {
                  setState(() {
                    _formKey.currentState.reset();
                    register = !register;
                  });
                },
                child: Text(register ? "Login" : "Register"),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Container customTextField(
    String label,
    IconData icon, {
    String value,
    TextEditingController controller,
    Function(String) onSaved,
  }) =>
      Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        width: 300,
        child: TextFormField(
            decoration: InputDecoration(
              focusColor: lightGrey,
              prefixIcon: Icon(icon),
              prefixStyle: TextStyle(
                color: Colors.yellow.shade400,
              ),
              labelText: label,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(
                  color: light,
                ),
              ),
            ),
            controller: controller,
            onSaved: onSaved,
            validator: validate),
      );

  onPressed() async {
    try {
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        setState(() {
          _buttonPressed = !_buttonPressed;
        });
        if (register) {
          print("register");
          Provider.of<Auth>(context, listen: false)
              .register(
            _formData['username'].toString(),
            _formData['email'].toString(),
            _formData['password'].toString(),
          )
              .then(
            (value) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => UsersScreen(),
                ),
              );
            },
          );
        } else {
          // var login =
          Provider.of<Auth>(context, listen: false).login(
            _formData['email'].toString(),
            _formData['password'].toString(),
          );
          await Future.delayed(Duration(seconds: 1));
          if (authId != null) {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (_) => UsersScreen(),
              ),
            );
          } else {
            _buttonPressed = !_buttonPressed;
            showDialog(
                context: context,
                builder: (ctx) => AlertDialog(
                      title: Text("Login Error occur"),
                      content: Text("Please input correct Details"),
                      actions: [
                        TextButton(
                          onPressed: () {
                            setState(() {
                              _buttonPressed = false;
                            });
                            Navigator.pop(context);
                          },
                          child: Text("cancel"),
                        )
                      ],
                    ));
          }
        }
      }
    }
    // on HttpException catch (error) {
    //   var errorMessage = "Authentication Failed";
    //   print(errorMessage);
    //   if (error.message == "error")
    //     showDialog(
    //       context: context,
    //       builder: (context) => AlertDialog(
    //         content: Text(error.message),
    //       ),
    //     );
    // }
    catch (error) {
      // var errorMessage = "could not Authenticate. try again later";
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          content: Text(error.message),
        ),
      );
    }
  }
}

String validate(String value) {
  if (value == "")
    return "fields are empty";
  else if (value.length < 6)
    return "Email or Password Incorrect";
  else
    return null;
}
