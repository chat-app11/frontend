import 'package:flutter/material.dart';
import 'package:frontend/responsive/style.dart';

class CustomBtn extends StatelessWidget {
  const CustomBtn({
    Key key,
    this.text,
    this.onPressed,
  });
  final String text;
  final Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      // margin: EdgeInsets.only(right: 30),
      child: ElevatedButton(
        onPressed: onPressed,
        child: Text(text.toUpperCase()),
        style: ElevatedButton.styleFrom(
            textStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
              letterSpacing: 2,
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            primary: active.withOpacity(.6),
            padding: EdgeInsets.symmetric(horizontal: 100, vertical: 20)),
      ),
    );
  }
}
