import 'package:flutter/material.dart';
import 'package:frontend/model/group.dart';
import 'package:frontend/model/message.dart';
import 'package:frontend/model/user.dart';
import 'package:frontend/model/user_list_group.dart';

import 'package:frontend/screens/chat_screen.dart';
import 'package:frontend/screens/group_chat_screen.dart';
import 'package:frontend/screens/login.dart';
import 'package:frontend/screens/profile_screen.dart';
import 'package:frontend/screens/add_user_to_group.dart';
import 'package:frontend/screens/users_screen.dart';

// import 'package:frontend/screens/chat_screen.dart';
// import 'package:frontend/screens/login.dart';
import 'package:provider/provider.dart';

import 'authentication/auth.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => Auth()),
        ChangeNotifierProvider(create: (_) => MessageProvider()),
        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => UserListGroup()),
        ChangeNotifierProvider(create: (_) => GroupProvider()),
      ],
      child: MaterialApp(
        title: 'Flutter + Laravel',
        theme: ThemeData(
          textTheme: GoogleFonts.latoTextTheme(
            Theme.of(context).textTheme,
          ),
        ),
        debugShowCheckedModeBanner: false,
        home: UsersScreen(),
        routes: {
          ChatScreen.routeName: (ctx) => ChatScreen(),
          AddUserToGroup.routeName: (ctx) => AddUserToGroup(),
          ProfileScreen.routeName: (ctx) => ProfileScreen(),
          GroupChatScreen.routeName : (ctx) => GroupChatScreen(),
        },
      ),
      // Responsiveness(ChatScreen())),
    );
  }
}
